﻿namespace EFLesson.Controllers
{
    using System.Linq;
    using System.Web.Http;
    using System.Data.Entity;

    [RoutePrefix("api/mountains")]
    public class MountainController : ApiController
    {
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetMountains()
        {
            using (var db = new LessonDbContext())
            {
                var mountains = db.Mountains.Include(x => x.Country).Select(x =>

                    new
                    {
                        Name = x.Name,
                        Height = x.Height,
                        CountryId = x.CountryId,
                        Country = new
                        {
                            Name = x.Country.Name
                        }
                    }).Take(3).ToArray();

                return Ok(mountains);
            }
        }
    }
}
