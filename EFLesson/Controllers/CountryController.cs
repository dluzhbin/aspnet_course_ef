﻿namespace EFLesson.Controllers
{
    using System.Linq;
    using System.Web.Http;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using EFLesson.DtoModels;
    using EFLesson.Models;

    [RoutePrefix("api/countries")]
    public class CountryController : ApiController
    {
        [HttpGet]
        [Route("")]
        public IHttpActionResult Get()
        {
            using (var db = new LessonDbContext())
            {
                var countries = db.Countries.Where(x =>
                    x.Name.StartsWith("Бу")).OrderBy(x => x.Name).Skip(1).Take(1).Select(
                    x => new CountryModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        ShortName = x.ShortName
                    }).ToArray();

                return Ok(countries);
            }
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult Post(CountryModel model)
        {
            using (var db = new LessonDbContext())
            {
                var newCountry = new Country()
                {
                    Name = model.Name,
                    ShortName = model.ShortName
                };
                db.Countries.AddOrUpdate(newCountry);
                db.SaveChanges();

                return Created($"countries/{newCountry.Id}", newCountry);
            }
        }
    }
}
