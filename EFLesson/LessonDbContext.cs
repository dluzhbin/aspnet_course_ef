﻿namespace EFLesson
{
    using System.Data.Entity;
    using System.Reflection;
    using EFLesson.Models;

    public class LessonDbContext : DbContext
    {
        public LessonDbContext() : base("LessonConnectionString")
        {
            Database.SetInitializer<LessonDbContext>(
                new MigrateDatabaseToLatestVersion<LessonDbContext,Migrations.Configuration>());

            #if DEBUG
            Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
            #endif
        }

        public DbSet<Country> Countries { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Mountain> Mountains { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Configuration.LazyLoadingEnabled = true;
            
            ConfigureRelationships(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        private void ConfigureRelationships(DbModelBuilder builder)
        {
            var countryBuilder = builder.Entity<Country>();
            countryBuilder.Property(x => x.Name).HasMaxLength(256).IsRequired();
            countryBuilder.HasMany(x => x.Regions).WithOptional().HasForeignKey(x => x.CountryId);
            countryBuilder.HasMany(x => x.Mountains).WithOptional().HasForeignKey(x => x.CountryId).WillCascadeOnDelete(false);

            var regionBuilder = builder.Entity<Region>();
            regionBuilder.Property(x => x.Name).HasMaxLength(256).IsRequired();
            regionBuilder.HasMany(x => x.Mountains).WithOptional().HasForeignKey(x => x.RegionId).WillCascadeOnDelete(false); ;

            var climberBuilder = builder.Entity<Climber>();
            climberBuilder.Property(x => x.Name).HasMaxLength(256).IsRequired();
            climberBuilder.Property(x => x.Address).HasMaxLength(512).IsRequired();

            var mountainBuilder = builder.Entity<Mountain>();
            mountainBuilder.Property(x => x.Name).HasMaxLength(256).IsRequired();
            mountainBuilder.Property(x => x.Height).IsRequired();
            mountainBuilder.Property(x => x.Latitude).IsRequired();
            mountainBuilder.Property(x => x.Longitude).IsRequired();
            mountainBuilder.HasMany(x => x.Ascents).WithOptional().HasForeignKey(x => x.MountainId);

            var ascentBuilder = builder.Entity<Ascent>();
            ascentBuilder.Property(x => x.StartDate).IsRequired();
            ascentBuilder.Property(x => x.EndDate).IsRequired();
        }
    }
}