﻿namespace EFLesson.Models
{
    using System.Collections.Generic;

    public class Country : BaseEntity
    {
        public string Name { get; set; }
        public string ShortName { get; set; }

        public virtual ICollection<Region> Regions { get; set; }
        public virtual ICollection<Mountain> Mountains { get; set; }
    }
}