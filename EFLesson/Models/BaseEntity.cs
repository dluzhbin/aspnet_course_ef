﻿namespace EFLesson.Models
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public class BaseEntity
    {
        [JsonProperty("id")]
        public long Id { get; set; }
    }
}