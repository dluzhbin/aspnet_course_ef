﻿namespace EFLesson.Models
{
    public class AcsentClimber
    {
        public long AscentId { get; set; }
        public long ClimberId { get; set; }

        public virtual Ascent Ascent { get; set; }
        public virtual Climber Climber { get; set; }
    }
}