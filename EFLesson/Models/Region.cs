﻿using System.Collections.Generic;

namespace EFLesson.Models
{
    public class Region : BaseEntity
    {
        public string Name { get; set; }
        public long CountryId { get; set; }

        public virtual Country Country {get; set; }

        public virtual ICollection<Mountain> Mountains { get; set; }
    }
}