﻿using System.Collections.Generic;

namespace EFLesson.Models
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public class Mountain : BaseEntity
    {
        public string Name { get; set; }
        public int Height { get; set; }
        public long? CountryId { get; set; }
        public long? RegionId { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }

        public virtual Country Country { get; set; }

        public virtual Region Region { get; set; }
        
        public virtual ICollection<Ascent> Ascents { get; set; }
    }
}