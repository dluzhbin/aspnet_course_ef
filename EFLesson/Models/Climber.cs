﻿namespace EFLesson.Models
{
    public class Climber : BaseEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}