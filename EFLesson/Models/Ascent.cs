﻿namespace EFLesson.Models
{
    using System;

    public class Ascent : BaseEntity
    {
        public long MountainId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public virtual Mountain Mountain { get; set; }
    }
}