﻿namespace EFLesson.DtoModels
{
    public class CountryModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }
}